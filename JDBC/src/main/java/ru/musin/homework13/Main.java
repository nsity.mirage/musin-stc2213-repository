package ru.musin.homework13;

import java.util.List;
import java.util.Optional;

public class Main {
    public static void main(String[] args) {
        CrudHuman crudHuman = new CrudHumanImpl();
        for (int i = 1; i < 10; i++) {
            Human human = new Human(i, "name" + i, "lastName" + i, "patronymic" + i, "sity" + i, "street" + i, "house" + i, "flat" + i, "numberPassport" + i);
            crudHuman.createHuman(human);
        }
        Optional<List<Human>> humans = crudHuman.getAllHumans(); //теперь в humans весь список людей из бд

        Optional<Human> human = crudHuman.getHumanById(4); //теперь в human находится конкретный человек (если такого id нет, то решение проблемы - на Ваше усмотрение)
        System.out.println(human.isPresent() ? human : "Не существует");
        human.get().setName("Mitrofan");
        crudHuman.updateHuman(human.get()); // теперь значения у данного человека в бд должно измениться
        crudHuman.deleteHuman(human.get()); //после выполнения данной команды - данный человек должен удалиться из бд
    }
}

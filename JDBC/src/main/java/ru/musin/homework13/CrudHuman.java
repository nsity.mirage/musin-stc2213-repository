package ru.musin.homework13;

import java.util.List;
import java.util.Optional;

public interface CrudHuman {
    Optional<List<Human>> getAllHumans();

    Optional<Human> getHumanById(int i);

    void createHuman(Human human);

    void updateHuman(Human human);

    void deleteHuman(Human human);
}

package ru.musin.homework13;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

public class CrudHumanImpl implements CrudHuman {
    private static final Logger log = java.util.logging.Logger.getLogger(CrudHumanImpl.class.getName());
    private final Connection connection = DBUtil.createConnection();

    @Override
    public Optional<List<Human>> getAllHumans() {
        final String GET_ALL_HUMAN = "SELECT * FROM People";
        final List<Human> people = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(GET_ALL_HUMAN);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Human human = humanMapper(resultSet);
                people.add(human);
            }
        } catch (SQLException e) {
            log.info("List size = " + people.size() + "," + "SQLException: " + e.getMessage());
            return Optional.empty();
        }
        return Optional.of(people);
    }

    @Override
    public Optional<Human> getHumanById(int id) {
        final String GET_HUMAN_BY_ID = "SELECT * FROM People WHERE id = ?";
        Optional<Human> human = Optional.empty();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(GET_HUMAN_BY_ID);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            while ((resultSet.next())) {
                human = Optional.of(humanMapper(resultSet));
            }
        } catch (SQLException e) {
            log.info("SQLException: " + e.getMessage());
            return Optional.empty();
        }
        return human;
    }

    @Override
    public void createHuman(Human human) {
        final String CREATE_HUMAN = "INSERT INTO People (name,lastName,patronymic,city,street,house,flat,numberPassport,id) VALUES (?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(CREATE_HUMAN);
            humanMapper(preparedStatement, human);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.info("SQLException: " + e.getMessage());
        }
    }

    @Override
    public void updateHuman(Human human) {

        String UPDATE_HUMAN = "UPDATE People SET name=?, lastName=?, patronymic=?,city=?,street=?,house=?,flat=?,numberPassport=? where id=?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_HUMAN);
            humanMapper(preparedStatement, human);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.info("SQLException: " + e.getMessage());
        }
    }

    @Override
    public void deleteHuman(Human human) {
        final String DELETE_HUMAN = "DELETE FROM People WHERE id=?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_HUMAN);
            preparedStatement.setInt(1, human.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.info("SQLException: " + e.getMessage());
        }

    }

    private Human humanMapper(ResultSet resultSet) throws SQLException {
        Human human = new Human();
        human.setId(resultSet.getInt("id"));
        human.setName(resultSet.getString("name"));
        human.setLastName(resultSet.getString("lastName"));
        human.setPatronymic(resultSet.getString("patronymic"));
        human.setCity(resultSet.getString("city"));
        human.setStreet(resultSet.getString("street"));
        human.setHouse(resultSet.getString("house"));
        human.setFlat(resultSet.getString("flat"));
        human.setNumberPassport(resultSet.getString("numberPassport"));
        return human;
    }

    private void humanMapper(PreparedStatement preparedStatement, Human human) throws SQLException {
        Human humanOriginal = getHumanById(human.getId()).orElse(human);
        preparedStatement.setString(1, human.getName() != null ? human.getName() : humanOriginal.getName());
        preparedStatement.setString(2, human.getLastName() != null ? human.getLastName() : humanOriginal.getLastName());
        preparedStatement.setString(3, human.getPatronymic() != null ? human.getPatronymic() : humanOriginal.getPatronymic());
        preparedStatement.setString(4, human.getCity() != null ? human.getCity() : humanOriginal.getCity());
        preparedStatement.setString(5, human.getStreet() != null ? human.getStreet() : humanOriginal.getStreet());
        preparedStatement.setString(6, human.getHouse() != null ? human.getHouse() : humanOriginal.getHouse());
        preparedStatement.setString(7, human.getFlat() != null ? human.getFlat() : humanOriginal.getFlat());
        preparedStatement.setString(8, human.getNumberPassport() != null ? human.getNumberPassport() : humanOriginal.getNumberPassport());
        preparedStatement.setInt(9, human.getId());
    }
}

package ru.musin.homework14.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.musin.homework14.models.Teacher;

@Repository
public interface TeacherRepository extends JpaRepository<Teacher, Integer> {
}

package ru.musin.homework14;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.musin.homework14.models.Student;
import ru.musin.homework14.models.Teacher;
import ru.musin.homework14.services.StudentService;
import ru.musin.homework14.services.TeacherService;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;

@SpringBootApplication
public class SpringJpaAndHibernateApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringJpaAndHibernateApplication.class, args);
        TeacherService teacherService = ApplicationContextHolder.getApplicationContext().getBean(TeacherService.class);
        StudentService studentService = ApplicationContextHolder.getApplicationContext().getBean(StudentService.class);

        Teacher teacher1 = teacherService.findById(1);
        Teacher teacher2 = teacherService.findById(2);

        Student student1 = studentService.findById(1);
        Student student2 = studentService.findById(2);
        Student student3 = studentService.findById(3);

        teacher1.setStudents(new HashSet<>(Arrays.asList(student1, student2)));
        teacher2.setStudents(new HashSet<>(Collections.singletonList(student3)));

        student1.setTeachers(new HashSet<>(Collections.singletonList(teacher1)));
        student2.setTeachers(new HashSet<>(Collections.singletonList(teacher1)));
        student3.setTeachers(new HashSet<>(Arrays.asList(teacher1, teacher2)));

        teacherService.saveAllAndFlush(Arrays.asList(teacher1, teacher2));
    }
}

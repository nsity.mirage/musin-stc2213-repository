package ru.musin.homework14.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.musin.homework14.exceptions.UserNotFoundException;
import ru.musin.homework14.models.Student;
import ru.musin.homework14.repositories.StudentRepository;

@Service
@Slf4j
@RequiredArgsConstructor
public class StudentService {

    private final StudentRepository studentRepository;


    public Student findById(int id) {
        return studentRepository.findById(id).orElseThrow(() -> new UserNotFoundException("Студент не найден по id: " + id));
    }
}

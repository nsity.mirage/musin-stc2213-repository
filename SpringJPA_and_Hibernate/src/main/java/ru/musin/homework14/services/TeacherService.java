package ru.musin.homework14.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.musin.homework14.exceptions.UserNotFoundException;
import ru.musin.homework14.models.Teacher;
import ru.musin.homework14.repositories.TeacherRepository;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class TeacherService {
    private final TeacherRepository teacherRepository;


    public Teacher findById(int id) {
        return teacherRepository.findById(id).orElseThrow(() -> new UserNotFoundException("Учитель не найден по id: " + id));
    }

    public void saveAllAndFlush(List<Teacher> asList) {
        teacherRepository.saveAllAndFlush(asList);
    }
}

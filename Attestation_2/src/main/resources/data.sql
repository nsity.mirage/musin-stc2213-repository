INSERT INTO client (name)
VALUES ('client_1');
INSERT INTO client (name)
VALUES ('client_2');
INSERT INTO client (name)
VALUES ('client_3');

INSERT INTO item (description, price, amount, order_id)
VALUES ('Iphone', 100000, 10, NULL);
INSERT INTO item (description, price, amount, order_id)
VALUES ('Book', 1400, 10, NULL);
INSERT INTO item (description, price, amount, order_id)
VALUES ('Laptop', 23000, 10, NULL);
INSERT INTO item (description, price, amount, order_id)
VALUES ('Notebook', 35000, 10, NULL);
INSERT INTO item (description, price, amount, order_id)
VALUES ('Monitor', 15000, 10, NULL);

INSERT INTO "order" (client_id, date, count_item)
VALUES (1, NOW(), 2);
UPDATE item
SET order_id = 1
WHERE description = 'Iphone';
UPDATE item
SET order_id = 1
WHERE description = 'Book';

INSERT INTO "order" (client_id, date, count_item)
VALUES (2, NOW(), 1);
UPDATE item
SET order_id = 2
WHERE description = 'Laptop';

INSERT INTO "order" (client_id, date, count_item)
VALUES (3, NOW(), 2);
UPDATE item
SET order_id = 3
WHERE description = 'Notebook';
UPDATE item
SET order_id = 3
WHERE description = 'Monitor';


package ru.musin.attestation_1.service;

import ru.musin.attestation_1.model.User;

public interface UserRepositoryFile {

    /**
     * Поиск пользователя по id
     *
     * @param id - идентификатор пользователя
     * @return найденного пользователя по id.
     */
    User findById(int id);

    /**
     * Создает пользователя
     *
     * @param user - пользователь, которого нужно создать
     */
    void create(User user);

    /**
     * Заменяет данные пользователя
     *
     * @param user - пользователь с новыми данными, которые нужно обновить
     */
    void update(User user);

    /**
     * Удаляет пользователя
     *
     * @param id - идентификатор пользователя
     */
    void delete(int id);
}

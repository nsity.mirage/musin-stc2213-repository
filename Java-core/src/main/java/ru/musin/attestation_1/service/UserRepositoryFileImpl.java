package ru.musin.attestation_1.service;

import ru.musin.attestation_1.model.User;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class UserRepositoryFileImpl implements UserRepositoryFile {
    private final File file = new File("src/main/java/ru/musin/attestation_1/Users.txt");
    private final Map<Integer, User> users = getUsers();

    @Override
    public User findById(int id) {
        return users.getOrDefault(id, null);
    }

    @Override
    public void create(User user) {
        if (!users.containsKey(user.getId())) {
            users.put(user.getId(), user);
            saveUsersToFile();
        } else
            System.out.println("Пользователь с таким id уже существует");
    }

    @Override
    public void update(User user) {
        users.compute(user.getId(), (key, value) -> value);
        saveUsersToFile();
    }

    @Override
    public void delete(int id) {
        if (users.containsKey(id)) {
            users.remove(id);
            saveUsersToFile();
        } else System.out.println("Пользователь не найден по id.");

    }

    private void saveUsersToFile() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            for (User user : users.values()) {
                writer.write(user.getId() + "|" + user.getName() + "|" + user.getAge() + "\n");
                writer.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private HashMap<Integer, User> getUsers() {
        HashMap<Integer, User> getUsers = new HashMap<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String strUser;
            while ((strUser = reader.readLine()) != null) {
                int id = Integer.parseInt(strUser.split("\\|")[0]);
                String name = strUser.split("\\|")[1];
                int age = Integer.parseInt(strUser.split("\\|")[2]);
                getUsers.put(id, new User(id, name, age));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return getUsers;

    }
}

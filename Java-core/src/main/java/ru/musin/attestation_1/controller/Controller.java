package ru.musin.attestation_1.controller;

import ru.musin.attestation_1.model.User;
import ru.musin.attestation_1.service.UserRepositoryFile;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Controller {

    private final List<String> commands = Arrays.asList("find", "create", "update", "delete");
    private final UserRepositoryFile userRepositoryFile;

    public Controller(UserRepositoryFile userRepositoryFile) {
        this.userRepositoryFile = userRepositoryFile;
    }

    public void handleCommand(final String command, final Scanner scanner) {
        switch (command) {
            case "find":
                System.out.println("Введите id пользователя для поиска");
                int id = scanner.nextInt();
                User user = userRepositoryFile.findById(id);
                System.out.println(user != null ? user : "Пользователь не найден");
                break;
            case "create":
                System.out.println("Заполните поля");
                userRepositoryFile.create(requestUser(scanner));
                break;
            case "update":
                user = requestUser(scanner);
                userRepositoryFile.update(user);
                break;
            case "delete":
                System.out.println("Укажите id пользователя, которого нужно удалить");
                id = scanner.nextInt();
                userRepositoryFile.delete(id);
                break;
            case "exit":
                return;
            default:
                System.out.println("Команда не найдена");
                break;
        }
    }

    private User requestUser(Scanner scanner) {
        System.out.println("Введите id");
        int id = scanner.nextInt();
        System.out.println("Введите имя");
        String name = scanner.next();
        System.out.println("Введите возраст");
        int age = scanner.nextInt();
        return new User(id, name, age);
    }

    public List<String> getCommands() {
        return commands;
    }
}

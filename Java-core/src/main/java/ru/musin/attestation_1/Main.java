package ru.musin.attestation_1;

import ru.musin.attestation_1.controller.Controller;
import ru.musin.attestation_1.service.UserRepositoryFile;
import ru.musin.attestation_1.service.UserRepositoryFileImpl;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        UserRepositoryFile repositoryFile = new UserRepositoryFileImpl();
        Controller controller = new Controller(repositoryFile);
        System.out.println("Данная программа поддерживает следующие консольные команды:");
        controller.getCommands().forEach(System.out::println);
        System.out.println("Введите одну из выше перечисленных команд. Для выхода введите exit");

        Scanner scanner = new Scanner(System.in);
        String command;
        while (!(command = scanner.next()).equals("exit")) {
            controller.handleCommand(command, scanner);
            System.out.println("Операция выполнена. Введите следующую команду или Exit для выхода");
        }
        scanner.close();
    }
}

package ru.musin.homework08;

public enum Profession {

    DEVELOPER("\"Разработчик\""),
    DEVOPS("\"ДевОпс\""),
    HR_MANAGER("\"HR-менеджер\""),
    TESTER("\"Тестировщик\"");

    private final String profession;

    Profession(String profession) {
        this.profession = profession;
    }

    @Override
    public String toString() {
        return profession;
    }
}

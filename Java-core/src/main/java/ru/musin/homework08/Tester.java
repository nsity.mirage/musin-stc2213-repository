package ru.musin.homework08;

public class Tester extends Worker {
    private final Profession tester = Profession.TESTER;

    public Tester(String name, String lastName) {
        super(name, lastName, Profession.TESTER);
    }

    @Override
    public void goToWork() {
        super.goToWork();
        System.out.printf("Задача инженеров профессии %s: " +
                "Тестирование и выявление ошибок продукта\n", tester);
    }

    @Override
    public void goToVacation(int days) {
        super.goToVacation(days);
        System.out.println("Количество дней отпуска, не должно превышать 2 недели в месяц.\n");
    }
}

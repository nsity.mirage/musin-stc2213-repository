package ru.musin.homework08;

public class Developer extends Worker {
    private final Profession developer = Profession.DEVELOPER;

    public Developer(String name, String lastName) {
        super(name, lastName, Profession.DEVELOPER);
    }

    @Override
    public void goToWork() {
        super.goToWork();
        System.out.printf("Задача инженеров профессии %s: " +
                "Разработка программного обеспечения\n", developer);
    }

    @Override
    public void goToVacation(int days) {
        super.goToVacation(days);
        System.out.println("Можно отдыхать все имеющиеся дни подряд, но будь готов в случае необходимости " +
                "подключится к работе удаленно, даже если ты на островах\n\n");
    }
}

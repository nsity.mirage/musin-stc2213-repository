package ru.musin.homework08;

public class Worker {
    private final String name;
    private final String lastName;
    private final Profession profession;

    public Worker(String name, String lastName, Profession profession) {
        this.name = name;
        this.lastName = lastName;
        this.profession = profession;
    }

    public void goToWork() {
        System.out.printf("\tРаботник: %s %s, профессия: %s\n", name, lastName, profession);
    }

    public void goToVacation(int days) {
        System.out.printf("\tИмя: %s %s, профессия: %s, количество дней отпуска: %d\n",
                name, lastName, profession, days);
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public Profession getProfession() {
        return profession;
    }
}

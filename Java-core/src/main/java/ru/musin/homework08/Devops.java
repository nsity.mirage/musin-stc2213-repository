package ru.musin.homework08;

public class Devops extends Worker {

    private final Profession devOps = Profession.DEVOPS;

    public Devops(String name, String lastName) {
        super(name, lastName, Profession.DEVOPS);
    }

    @Override
    public void goToWork() {
        super.goToWork();
        System.out.printf("Задача инженеров профессии %s: " +
                "оптимизировать отдельные процессы в компании, такие как:\n" +
                "разработка - сборка - тестирование - релиз - развертывание - управление - мониторинг\n", devOps);
    }

    @Override
    public void goToVacation(int days) {
        super.goToVacation(days);
        System.out.println("Количество дней отпуска не должно превышать одной недели в месяц.\n");
    }
}

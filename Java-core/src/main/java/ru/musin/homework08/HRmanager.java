package ru.musin.homework08;

public class HRmanager extends Worker {
    private final Profession hrManager = Profession.HR_MANAGER;

    public HRmanager(String name, String lastName) {
        super(name, lastName, Profession.HR_MANAGER);
    }

    @Override
    public void goToWork() {
        super.goToWork();
        System.out.printf("Задача специалистов профессии %s: " +
                "Поиск сотрудников для IT-компании\n", hrManager);
    }

    @Override
    public void goToVacation(int days) {
        super.goToVacation(days);
        System.out.println("\tПривел Senior-разработчика - в год можешь взять целый месяц,\n" +
                " а если нет - извиняй, разбивай дни по неделю в месяц.\n");
    }
}

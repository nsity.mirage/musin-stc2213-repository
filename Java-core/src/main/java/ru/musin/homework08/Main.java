package ru.musin.homework08;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Worker> workers = new ArrayList<>();
        Developer developer = new Developer("Денис", "Мусин");
        HRmanager hrManager = new HRmanager("Анатолий", "Толян");
        Devops devops = new Devops("Борис", "Борян");
        Tester tester = new Tester("Артур", "Артурян");
        workers.add(developer);
        workers.add(hrManager);
        workers.add(devops);
        workers.add(tester);

        workers.stream().forEach(worker -> {
            worker.goToWork();
            worker.goToVacation(28);
        });
    }
}

package ru.musin.homework10;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

public class Main {

    public static void main(String[] args) {
        final int[] randomNumbers = generate();
        Arrays.stream(randomNumbers).forEach(x -> System.out.print(x + " "));

        System.out.println("\nПроверка на четность элемента:");
        Arrays.stream(Sequence.filter(randomNumbers, x -> x % 2 == 0))
                .forEach(x -> System.out.print(x + " "));

        System.out.println("\nПроверка, является ли сумма цифр элемента четным числом:");
        Arrays.stream(Sequence.filter(randomNumbers,
                x -> Arrays.stream(Integer.toString(x).split(""))
                        .mapToInt(Integer::parseInt)
                        .sum() % 2 == 0
        )).forEach(x -> System.out.print(x + " "));

        System.out.println("\nПроверка на четность всех цифр числа:");
        Arrays.stream(Sequence.filter(randomNumbers,
                x -> Arrays.stream(Integer.toString(x).split(""))
                        .mapToInt(Integer::parseInt)
                        .allMatch(i -> i % 2 == 0)
        )).forEach(x -> System.out.print(x + " "));

        System.out.println("\nПроверка на палиндромность числа:");
        Arrays.stream(Sequence.filter(randomNumbers, x ->
                {
                    String numb = String.valueOf(x);
                    if (numb.length() < 2) return true;
                    StringBuilder s1 = new StringBuilder(numb.substring(0, (int) Math.floor(numb.length() / 2.0)));
                    double m2 = Math.ceil(numb.length() / 2.0);
                    StringBuilder s2 = new StringBuilder(numb.substring((int) m2));
                    s2.reverse();
                    return s1.toString().equals(s2.toString());
                }
        )).forEach(x -> System.out.print(x + " "));


    }

    private static int[] generate() {
        return IntStream.range(0, 100)
                .boxed()
                .map(i -> ThreadLocalRandom.current().nextInt(2100, 2250))
                .mapToInt(Integer::intValue).toArray();
    }
}

package ru.musin.homework07;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random random = new Random();
        int numberOfPeople = random.nextInt(20);
        Human[] humans = new Human[numberOfPeople];

        for (int i = 0; i < humans.length; i++) {
            humans[i] = new Human("Human", "#" + i, random.nextInt(90));
        }

        Arrays.sort(humans, Comparator.comparingInt(Human::getAge));

        for (Human temp : humans) {
            System.out.println(temp);
        }
    }
}

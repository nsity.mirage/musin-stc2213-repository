package ru.musin.homework05;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Task5 {

    /*
     Метод (функция) возвращает индекс в массиве, указанного числа.
     */
    private static int findIndex(List<Integer> array, int number) {
        int index = -1;
        for (int i = 0; i < array.size(); i++) {
            if (array.get(i) == number) {
                index = i;
                return index;
            }

        }
        return index;
    }

    /*
    Метод переводит все значимые элементы влево.
     */
    private static ArrayList<Integer> sortArray(List<Integer> numbers) {
        ArrayList<Integer> sortNumbers = new ArrayList<>();
        for (Integer number : numbers) {
            if (number == 0) {
                sortNumbers.add(sortNumbers.size(), number);
            } else sortNumbers.add(0, number);
        }
        return sortNumbers;
    }

    public static void main(String[] args) {
        Scanner inConsole = new Scanner(System.in);
        List<Integer> numbers = new ArrayList<>();
        System.out.println("Введите последовательность чисел. Для окончания ввода, введите -1");
        int temp = inConsole.nextInt();
        while (temp != -1) {
            numbers.add(temp);
            temp = inConsole.nextInt();
        }
        System.out.println("Введите число, индекс которого нужно получить");
        int number = inConsole.nextInt();

        System.out.printf("Индекс: %d\n", findIndex(numbers, number));
        //сортировка нулей в право
        System.out.println(numbers);
        System.out.println(sortArray(numbers));
    }

}

package ru.musin.homework03;

public class Task1 {
    /*
   Вычислить значение выражения "столбиком" в двоичной системе счисления:
   121(10) - 68(10)
    */
    public static void main(String[] args) {
        byte x = 121;
        int y = -68;

        String binaryX = getBinaryNum(x);
        String binaryY = getBinaryNum(y);
        //121(10) - 68(10) = 121 + (-68) -> вызываем метод для расчета суммы.
        String summ = sumBinary(binaryX,binaryY);

        System.out.println("Результат в двоичном представлении: " + summ);
        System.out.println("Результат в десятичном представлении: " + binaryToInt(summ));
    }
//Метод переводит из десятичной системы в двоичную.
    private static String getBinaryNum(int num) {
        String binaryNum = "";
        if (num >= 0) {
            while (num != 0) {
                binaryNum = num % 2 + binaryNum;
                num = num / 2;
            }
            binaryNum = 0 + binaryNum;
        } else {
            num = num * -1;
            String bin = getBinaryNum(num);
            binaryNum = reverseCode(bin);
            binaryNum = sumBinary(binaryNum, "00000001");
        }
        String result = String.format(binaryNum);
        return result;
    }
//Обратный код. Заменяет все 0 на 1 и 1 на 0.
    private static String reverseCode(String binaryNum) {
        String result = "";
        char[] array = binaryNum.toCharArray();
        for (int i = 0; i < array.length; i++) {
            if (array[i] == '1') result += 0;
            else result += 1;
        }
        return result;
    }

    private static String sumBinary(String a, String b) {
        String result = "";
        boolean flag = false;
        char[] binaryA = a.toCharArray();
        char[] binaryB = b.toCharArray();
        for (int i = binaryA.length - 1; i >= 0; i--) {
            if (binaryA[i] == '1' & binaryB[i] == '1') {
                result = flag ? "1" + result : "0" + result;
                flag = true;
            } else if ((binaryA[i] == '1' & binaryB[i] == '0') || (binaryA[i] == '0' & binaryB[i] == '1')) {
                result = flag ? "0" + result : "1" + result;
            } else if (binaryA[i] == '0' & binaryB[i] == '0') {
                result = flag ? "1" + result : "0" + result;
                flag = false;
            }
        }
        return result;

    }

    private static int binaryToInt(String binaryNum) {
        int result = 0;
        char[] array = binaryNum.toCharArray();
        int count = 0;
        for (int i = array.length - 1; i > 0; i--) {
            if (array[i] == '1') result += Math.pow(2, count);
            count++;
        }
        return array[0] == '1' ? result * -1 : result;
    }
}

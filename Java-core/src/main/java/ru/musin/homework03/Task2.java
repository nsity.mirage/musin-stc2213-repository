package ru.musin.homework03;

import java.util.Scanner;

public class Task2 {

    public static void main(String[] args) {
        int min;
        int count_even = 0;
        int count_odd = 0;
        Scanner scanner = new Scanner(System.in);

        int temp = scanner.nextInt();
        min = temp;
        while (temp != -1) {
            if (temp % 2 == 0) count_even++;
            else count_odd++;
            if (temp < min) min = temp;
            temp = scanner.nextInt();
        }
        System.out.printf(
                "Минимальное число: %d" + "\n" +
                        "Количество четных чисел: %d" + "\n" +
                        "Количество нечетных чисел: %d", min, count_even, count_odd);
    }
}

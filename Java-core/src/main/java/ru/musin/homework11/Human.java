package ru.musin.homework11;

public class Human {
    private final String name;
    private final String lastName;
    private final String patronymic;
    private final String city;
    private final String street;
    private final String house;
    private final String flat;
    private final String numberPassport;

    public Human(String lastName, String name, String patronymic, String city, String street, String house, String flat, String numberPassport) {
        this.name = name;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.city = city;
        this.street = street;
        this.house = house;
        this.flat = flat;
        this.numberPassport = numberPassport;
    }

    @Override
    public int hashCode() {
        return numberPassport.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || this.hashCode() != obj.hashCode()) return false;
        return obj instanceof Human && numberPassport.equals(((Human) obj).numberPassport);
    }

    @Override
    public String toString() {
        return String.format("%s %s %s\n" +
                        "Паспорт:\n" +
                        "Серия: %s номер: %s\n" +
                        "Город %s, ул. %s, дом %s, квартира %s\n",
                lastName, name, patronymic, numberPassport.substring(0, 4), numberPassport.substring(4),
                city, street, house, flat
        );
    }
}

package ru.musin.homework11;

public class Main {
    public static void main(String[] args) {
        Human human1 = new Human("Мусин", "Денис", "Борисович", "Казань",
                "Программистов", "27", "1", "9822897643");
        Human human2 = new Human("Мусин", "Денис", "Борисович", "Казань",
                "Программистов", "27", "1", "9822897643");
        Human human3 = new Human("Денис", "Мусин", "Борисович", "Java",
                "Разработчиков", "27", "1", "9822897643");
        System.out.println(human1);
        System.out.println(human1.equals(human2));
        System.out.println(human2.equals(human1));
        System.out.println(human1.equals(human3));
    }
}

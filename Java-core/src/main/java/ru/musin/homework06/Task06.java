package ru.musin.homework06;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Task06 {

    private static List<Integer> generate(final int size) {
        return IntStream.range(0, size)
                .boxed()
                .map(i -> ThreadLocalRandom.current().nextInt(-100, 101))
                .collect(Collectors.toList());
    }

    /*
    Метод возвращает массив, в котором:
    Индексы - мы будем принимать за сами числа от -100 до 100,
    но т.к. отрицательными быть не могут, прибавляем 100 для каждого числа.
    Значения - сколько раз мы встретили число (индекс) в последовательности.
     */
    private static int[] countNumbers(final List<Integer> numbers) {
        final int[] countNumbers = new int[201];
        for (int temp : numbers) {
            temp += 100;
            countNumbers[temp]++;
        }
        return countNumbers;
    }

    private static int minCountNumbers(final int[] countNumbers) {
        int minCount = Integer.MAX_VALUE;
        int index = -1;
        for (int i = 0; i < countNumbers.length; i++) {
            if (countNumbers[i] == 0) continue;
            if (countNumbers[i] < minCount) {
                minCount = countNumbers[i];
                index = i;
            }
        }
        return index - 100;
    }


    public static void main(String[] args) {
        //метод generate генерирует числа от -100 до 100
        final List<Integer> numbers = generate(10000);
        final int[] counts = countNumbers(numbers);
        System.out.println(minCountNumbers(counts));
    }
}

package ru.musin.homework12;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Main {

    private static Map<String, Long> countWords(final String str) {
        Map<String, Long> wordCount = Arrays.stream(str.trim().split(" "))
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        return wordCount;
    }

    public static void main(String[] args) {
        countWords("Маша и три медведя Маша").entrySet().forEach(System.out::println);
        countWords("А Б В Г А А В Г Г").entrySet().forEach(System.out::println);
    }

}

package ru.musin.homework09;

public class Ellipse extends Figure {
    public Ellipse(int x, int y) {
        super(x, y);
    }

    @Override
    int getPerimetr() {
        return (int) Math.round((4.0F * (Math.PI * getX() * getY()) + (getX() - getY())) / (getX() + getY()));
    }
}

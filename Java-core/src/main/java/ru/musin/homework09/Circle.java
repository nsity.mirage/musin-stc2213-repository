package ru.musin.homework09;

public class Circle extends Ellipse implements Moveable {

    public Circle(int x, int y) {
        super(x, y);
    }

    @Override
    public void move(int x, int y) {
        setX(getX() + x);
        setY(getY() + y);
    }
}

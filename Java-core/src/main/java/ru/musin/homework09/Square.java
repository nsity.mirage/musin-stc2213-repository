package ru.musin.homework09;

public class Square extends Rectangle implements Moveable {
    public Square(int x, int y) {
        super(x, y);
    }

    @Override
    public void move(int x, int y) {
        setX(getX() + x);
        setY(getY() + y);
    }
}

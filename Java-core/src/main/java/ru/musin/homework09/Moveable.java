package ru.musin.homework09;

public interface Moveable {

    void move(int x, int y);
}

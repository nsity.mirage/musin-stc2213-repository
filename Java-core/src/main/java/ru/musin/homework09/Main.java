package ru.musin.homework09;

public class Main {

    public static void main(String[] args) {
        Figure[] figures = new Figure[4];
        Moveable[] moveableFigures = new Moveable[2];
        Circle circle = new Circle(3, 3);
        Ellipse ellipse = new Ellipse(4, 3);
        Rectangle rectangle = new Rectangle(2, 4);
        Square square = new Square(4, 4);
        figures[0] = circle;
        figures[1] = ellipse;
        figures[2] = rectangle;
        figures[3] = square;
        moveableFigures[0] = circle;
        moveableFigures[1] = square;

        for (Figure figure : figures) {
            System.out.println(figure.getClass().getSimpleName() + ": " + figure.getPerimetr());
        }

        for (Moveable figure : moveableFigures) {
            figure.move(10, 10);
            if (figure instanceof Figure) {
                System.out.println(figure.getClass().getSimpleName() + ": " +
                        ((Figure) figure).getPerimetr());
            }
        }

    }
}

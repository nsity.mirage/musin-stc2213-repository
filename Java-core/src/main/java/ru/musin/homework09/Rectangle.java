package ru.musin.homework09;

public class Rectangle extends Figure {

    public Rectangle(int x, int y) {
        super(x, y);
    }

    @Override
    int getPerimetr() {
        return 2 * (getX() + getY());
    }
}
